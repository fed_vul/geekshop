from django.shortcuts import render
from .models import Product
from django.views.generic import ListView

# def index_view(request):

# 	same_product = Product.objects.all()

# 	context = {'same_product' : same_product}
	
# 	return render(request, 'mainapp/index.html', context)

class MainView(ListView):

	template_name = 'mainapp/index.html'

	model = Product


class ProductView(MainView):

	template_name = 'mainapp/products.html'

# def product_view(request):

# 	same_product = Product.objects.all()

# 	context = {'same_product' : same_product}
	
# 	return render(request, 'mainapp/products.html', context)


class ContactView(MainView):

	template_name = 'mainapp/contact.html'

# def contact_view(request):
# 	context = {'contact': 1}
# 	return render(request, 'mainapp/contact.html', context)


