from django.contrib import admin

from .models import ProductCategory, Product, Manufacturer, Contacts

# Register your models here.

# class PictureInline(admin.TabularInline):
#     model = Picture
#     fk_name = 'related_obj'
#     fields = ('title', 'active', 'image')


class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')
    # inlines = (PictureInline,)

admin.site.register(ProductCategory, ProductCategoryAdmin)


# class PictureAdmin(admin.ModelAdmin):
#     list_display = ('title', 'get_model_name', 'active', 'sort')
#     fields = ('title', 'active', 'image', 'sort', 'description', )

#     def get_model_name(self, obj):
#         return getattr(getattr(obj, 'related_obj', None), 'title', None) or ''

# admin.site.register(Picture, PictureAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort', 'image', 'category')
    # inlines = (PictureInline,)
    # exclude = ('pictures', )

admin.site.register(Product,ProductAdmin)

class ManufacturerAdmin(admin.ModelAdmin):
	list_display = ('title', 'description', 'active', 'country', 'sort')
	fields = ('title', 'description', 'active', 'country')

admin.site.register(Manufacturer, ManufacturerAdmin)


class ContactsAdmin(admin.ModelAdmin):
	list_display = ('title', 'description', 'active', 'region', 'phone' )
	fields = ('title', 'description', 'active', 'region', 'phone')

admin.site.register(Contacts, ContactsAdmin)


