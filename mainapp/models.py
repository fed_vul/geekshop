from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal

# Create your models here.
class Core(models.Model):
    title = models.CharField(max_length=250, default='', blank=True, null=False)
    description = models.TextField(_('dc-description'), null=True, blank=True)
    sort = models.IntegerField(_(u'sort'), default=0, blank=True, null=True)
    active = models.BooleanField(_(u'active'), default=False)

    def __str__(self):

        return f'{self.title}'

class ProductCategory(Core):
    """docstring for ProductCategory"""
    class Meta:
        verbose_name = _('Продуктовая категория')
        verbose_name_plural = _('Продуктовые категории')

    type = models.IntegerField(_('Тип категории'),default = 0, blank=True, null=False)

    

class Manufacturer(Core):
    """docstring for Manufacturer"""
    class Meta:
        verbose_name = _('Производитель')
        verbose_name_plural = _('Производители')


    country = models.CharField(max_length=50, default='',blank=True, null=False)

class Contacts(Core):
    """docstring for Contacts"""
    class Meta:
        verbose_name=_('Контакт')
        verbose_name_plural = _('Контакты')

    region = models.CharField(max_length=20, default='', blank=True, null=False)
    phone = models.CharField(max_length=20, default='', blank=True, null=False)  

class Product(Core):
    """docstring for Product"""
    class Meta:
        verbose_name = _('Продукт')
        verbose_name_plural = _('Продукты')

    category = models.ForeignKey(ProductCategory, blank=True,null = True, on_delete=models.SET_NULL)
    manufacturer_name = models.ForeignKey(Manufacturer, null=True, on_delete=models.SET_NULL)
    price = models.DecimalField(max_digits=7, decimal_places=2, blank=False, null = True)
    image=models.ImageField(upload_to='product_images', default = '')